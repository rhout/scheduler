# :calendar: Rest API for a scheduling app :calendar:

A simple REST API for scheduling meetings. This is an application that tries to live in the moment by only being able to create meetings for the current day.

## Running the application

You need to have Java JDK installed on your computer (e.g. [OpenJDK](https://openjdk.org/)) The application has only been tested with Java version 17.

If you want to use your terminal, navigate to the root of the project and run `./gradlew bootRun`.

### Endpoints:

- `POST /api/v1/persons` Create a person with name and (unique) email - Example `{
    "name": "Some Name",
    "email": "test@test.com"
}`
- `POST /api/v1/meetings` Create a meeting with a list of people (attendants) and a start time. The end time is automatically set to an hour after start time. - Example: `{
    "personIds": [
        "test@test.com"
    ],
    "startTime": "2024-02-05T12:00:00"
}`
- `GET /api/v1/schedule/{personId}` Get all the meetings for a specified person (the ID is the email)
- `POST /api/v1/suggestions` Show all the available time slots for the provided person ID's (which is an array of )
  - Example: `["test@test.com", "other@test.com"]`

## Remarks/shortcomings

Due to the scope of the task and the expected time spent on the task, several compromises have been made:

- The models should have a unique ID, but for the sake of simplicity, the unique email is used as ID for Person and omitted for Meeting.

- The error responses all give a `400 Bad Request`. This should be more granular, giving a correct response code and a message describing the error.

- I wanted to check if a time slot is already taken, but skipped it due to the lack of time.

- There are two test cases, one of them being very large. Usually I would make smaller unit tests with one or two assertion.
- The tests are very superficial and only test the basic functionality. There should be several more tests that test edge cases and when things go wrong.
