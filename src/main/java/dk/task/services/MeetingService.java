package dk.task.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import dk.task.models.Meeting;
import dk.task.models.Person;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class MeetingService {

    @Autowired
    private PersonService personService;

    private List<Meeting> meetings = new ArrayList<>();

    /**
     * Get all meetings by email.
     * 
     * @param personId
     * @return List<Meeting>
     */
    public List<Meeting> getMeetingsForPerson(String personId) {
        Person foundPerson = personService.getPersonByEmail(personId);

        List<Meeting> foundMeetings = null;

        if (foundPerson != null) {
            foundMeetings = meetings.stream().filter(
                    meeting -> meeting.getPersonIds().stream().anyMatch(id -> id.equals(personId)))
                    .collect(Collectors.toList());
        }

        return foundMeetings;
    }

    /**
     * Create a meeting with provided personIds and a start time.
     *
     * @param meeting
     * @return The created meeting or null, if the meeting could not be created or if it contains a
     *         person that does not exist.
     */
    public Meeting createMeeting(Meeting meeting) {
        boolean allPeopleInMeetingExist = meeting.getPersonIds().stream()
                .allMatch(email -> personService.getPersonByEmail(email) != null);

        LocalTime startTime = meeting.getStartTime();
        boolean startsAtHourMark = startTime.getMinute() == 0 && startTime.getSecond() == 0;

        if (allPeopleInMeetingExist && startsAtHourMark) {
            Meeting createdMeeting = new Meeting(meeting.getPersonIds(), startTime);
            meetings.add(createdMeeting);

            return createdMeeting;
        }

        return null;
    }

    /**
     * Gets a list of available time slots for the provided people
     * 
     * @param personIds Ids of the people to find a meeting for.
     * @return List<LocalTime>
     */
    public List<LocalTime> getMeetingSuggestions(List<String> personIds) {
        Stream<Meeting> meetingsForPeople = meetings.stream()
                .filter(meeting -> meeting.getPersonIds().stream().anyMatch(personIds::contains));

        List<LocalTime> takenStartTimes =
                meetingsForPeople.map(meeting -> meeting.getStartTime()).toList();

        List<LocalTime> availableTimeSlots = new ArrayList<>();

        for (LocalTime time = LocalTime.of(8, 0); time.isBefore(LocalTime.of(17, 0)); time =
                time.plusHours(1)) {
            if (!takenStartTimes.contains(time)) {
                availableTimeSlots.add(time);
            }
        }

        return availableTimeSlots;
    }
}
