package dk.task.services;

import org.springframework.stereotype.Service;

import dk.task.models.Person;

import java.util.ArrayList;
import java.util.List;

@Service
public class PersonService {

    private List<Person> persons = new ArrayList<>();

    /**
     * Get a person based on email.
     *
     * @param email
     * @return The person or null if it doesn't exist.
     */
    public Person getPersonByEmail(String email) {
        Person returnPerson = null;
        boolean exists = persons.stream().anyMatch(person -> person.getEmail().equals(email));

        if (exists) {
            returnPerson = persons.stream().filter(person -> person.getEmail().equals(email))
                    .findFirst().get();
        }

        return returnPerson;
    }

    /**
     * Adds a new person.
     *
     * @param person
     * @return The created person or null, if the person could not be created.
     */
    public Person createPerson(Person person) {
        Person createdPerson = null;
        if (!isEmailRegistered(person.getEmail())) {
            createdPerson = new Person(person.getName(), person.getEmail());
            persons.add(createdPerson);
        }

        return createdPerson;
    }



    /**
     * Checks if a person already exist by its email.
     * 
     * @param email The email to check against
     * @return boolean
     */
    private boolean isEmailRegistered(String email) {
        return persons.stream().filter(person -> person.getEmail().equals(email)).findAny()
                .isPresent();
    }
}
