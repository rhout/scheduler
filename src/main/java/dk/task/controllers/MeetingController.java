package dk.task.controllers;

import java.time.LocalTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import dk.task.models.Meeting;
import dk.task.models.DTOs.ScheduleDTO;
import dk.task.services.MeetingService;

@RestController
public class MeetingController {
    @Autowired
    private MeetingService meetingService;

    /**
     * Adds a new meeting to the "database".
     *
     * @param meeting
     * @return
     */
    @PostMapping("/meetings")
    public ResponseEntity<Meeting> createMeeting(@RequestBody Meeting meeting) {
        Meeting returnMeeting = meetingService.createMeeting(meeting);
        HttpStatus status = HttpStatus.BAD_REQUEST;

        if (returnMeeting != null) {
            status = HttpStatus.CREATED;
            return new ResponseEntity<>(returnMeeting, status);
        }
        return new ResponseEntity<>(status);
    }

    /**
     * Get all the meetings for a given person.
     * 
     * @param personId
     * @return ResponseEntity<ScheduleDTO>
     */
    @GetMapping("/schedule/{personId}")
    public ResponseEntity<ScheduleDTO> getSchedule(@PathVariable String personId) {
        List<LocalTime> meetingsForPerson = meetingService.getMeetingsForPerson(personId).stream()
                .map(meeting -> meeting.getStartTime()).toList();

        HttpStatus status = HttpStatus.BAD_REQUEST;

        if (meetingsForPerson != null) {
            status = HttpStatus.CREATED;
            return new ResponseEntity<>(new ScheduleDTO(personId, meetingsForPerson), status);
        }
        return new ResponseEntity<>(status);
    }

    /**
     * Shows all the free time slots for a number of people.
     * 
     * @param personIds
     * @return ResponseEntity<List<LocalTime>>
     */
    @PostMapping("/suggestions")
    public ResponseEntity<List<LocalTime>> getSuggestions(@RequestBody List<String> personIds) {
        HttpStatus status = HttpStatus.BAD_REQUEST;

        List<LocalTime> suggestions = meetingService.getMeetingSuggestions(personIds);

        if (!suggestions.isEmpty()) {
            status = HttpStatus.CREATED;
            return new ResponseEntity<>(suggestions, status);
        }
        return new ResponseEntity<>(status);
    }
}
