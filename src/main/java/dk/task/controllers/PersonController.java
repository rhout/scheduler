package dk.task.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import dk.task.models.Person;
import dk.task.services.PersonService;

@RestController
@RequestMapping("/persons")
public class PersonController {
    @Autowired
    private PersonService personService;

    /**
     * Adds a new person to the "database".
     *
     * @param person
     * @return
     */
    @PostMapping()
    public ResponseEntity<Person> createPerson(@RequestBody Person person) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        Person returnPerson = personService.createPerson(person);

        if (returnPerson != null) {
            status = HttpStatus.CREATED;
            return new ResponseEntity<>(returnPerson, status);
        }

        return new ResponseEntity<>(returnPerson, status);
    }
}
