package dk.task.models.DTOs;

import java.time.LocalTime;
import java.util.List;

public class ScheduleDTO {

    public String personId;
    public List<LocalTime> startTimes;

    public ScheduleDTO(String personName, List<LocalTime> meetings) {
        this.personId = personName;
        this.startTimes = meetings;
    }
}
