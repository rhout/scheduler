package dk.task.models;

import java.time.LocalTime;
import java.util.List;

public class Meeting {
    private LocalTime startTime;
    private LocalTime endTime;
    private List<String> personIds;

    public Meeting(List<String> personIds, LocalTime startTime) {
        this.startTime = startTime;
        endTime = startTime.plusHours(1);
        this.personIds = personIds;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public List<String> getPersonIds() {
        return personIds;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

}
