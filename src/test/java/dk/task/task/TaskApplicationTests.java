package dk.task.task;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.io.IOException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TaskApplicationTests {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	private String baseUrl;

	@BeforeEach
	public void setUp() {
		baseUrl = "http://localhost:" + port + "/api/v1";
	}

	@Test
	public void createDuplicatePersonGivesError() throws InterruptedException, IOException {
		ResponseEntity<String> response = createPerson("Test Name", "first@test.com");

		ResponseEntity<String> duplicatePersonResponse =
				createPerson("Test Name", "first@test.com");

		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertEquals(HttpStatus.BAD_REQUEST, duplicatePersonResponse.getStatusCode());
	}

	@Test
	public void createMeetingWithSeveralUsers() throws InterruptedException, IOException {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		String jsonBodyMeeting =
				"{\"personIds\": [\"test@test.com\", \"other@test.com\", \"third@test.com\"], \"startTime\": \"10:00:00\"}";
		HttpEntity<String> requestNewMeeting = new HttpEntity<>(jsonBodyMeeting, headers);

		createPerson("Test Name", "test@test.com");
		createPerson("Test Name", "other@test.com");
		createPerson("Test Name", "third@test.com");

		ResponseEntity<String> createMeetingResponse =
				restTemplate.postForEntity(baseUrl + "/meetings", requestNewMeeting, String.class);

		assertEquals(HttpStatus.CREATED, createMeetingResponse.getStatusCode());

		createSampleMeetings();

		ResponseEntity<String> getScheduleResponse =
				restTemplate.getForEntity(baseUrl + "/schedule/test@test.com", String.class);

		assertEquals(
				"{\"personId\":\"test@test.com\",\"startTimes\":[\"10:00:00\",\"13:00:00\",\"15:00:00\",\"08:00:00\"]}",
				getScheduleResponse.getBody());

		HttpEntity<String> requestSuggestions =
				new HttpEntity<>("[\"test@test.com\",\"other@test.com\"]", headers);
		ResponseEntity<String> getSuggestionsResponse = restTemplate
				.postForEntity(baseUrl + "/suggestions", requestSuggestions, String.class);

		System.out.println(getSuggestionsResponse.getBody());

		assertEquals("[\"09:00:00\",\"11:00:00\",\"12:00:00\",\"14:00:00\",\"16:00:00\"]",
				getSuggestionsResponse.getBody());
	}

	private ResponseEntity<String> createPerson(String name, String email) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		String personBody = "{\"name\": \"" + name + "\", \"email\": \"" + email + "\"}";
		HttpEntity<String> request1 = new HttpEntity<>(personBody, headers);

		return restTemplate.postForEntity(baseUrl + "/persons", request1, String.class);
	}

	private void createSampleMeetings() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		String jsonBodyMeeting1 =
				"{\"personIds\": [\"test@test.com\", \"third@test.com\"], \"startTime\": \"13:00:00\"}";
		String jsonBodyMeeting2 =
				"{\"personIds\": [\"test@test.com\"], \"startTime\": \"15:00:00\"}";
		String jsonBodyMeeting3 =
				"{\"personIds\": [\"test@test.com\"], \"startTime\": \"08:00:00\"}";

		HttpEntity<String> requestNewMeeting1 = new HttpEntity<>(jsonBodyMeeting1, headers);
		HttpEntity<String> requestNewMeeting2 = new HttpEntity<>(jsonBodyMeeting2, headers);
		HttpEntity<String> requestNewMeeting3 = new HttpEntity<>(jsonBodyMeeting3, headers);

		restTemplate.postForEntity(baseUrl + "/meetings", requestNewMeeting1, String.class);
		restTemplate.postForEntity(baseUrl + "/meetings", requestNewMeeting2, String.class);
		restTemplate.postForEntity(baseUrl + "/meetings", requestNewMeeting3, String.class);
	}
}
